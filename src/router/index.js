import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';

import routes from './routes';

Vue.use(VueRouter);

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  });

  Router.beforeEach(async (to, from, next) => {
    const GitAuthCode = to.query.code;

    await setGithubToken(GitAuthCode);

    const isAuth = await isAuthenticated();

    if (to.matched.some((record) => record.meta.forAuth) && isAuth) {
      return next({ path: '/' });
    }

    if (to.matched.some((record) => record.meta.forVisitors) && !isAuth) {
      return next({ path: '/login' });
    }

    return next();
  });

  return Router;
}

async function setGithubToken(code) {
  if (!code) return false;

  const githubAuth = await axios.post('api|github-auth/token', { code })
    .catch(() => false);

  if (!githubAuth) return false;

  const { token, type } = githubAuth.data;

  return localStorage.setItem('git_token', `${type} ${token}`);
}

async function isAuthenticated() {
  return !!localStorage.getItem('git_token');
}
